package praktikum15;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Lumehelves2 {
	
	int x, y;
	
	public Lumehelves2(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void joonistaMind(GraphicsContext gc) {
		gc.fillText("Joonista siia lumehelves", x, y);
		gc.setStroke(Color.BLUE);
		gc.setLineWidth(2);
		gc.strokeLine(x - 10, y - 10, x + 10, y + 10);
		gc.strokeLine(x + 10, y - 10, x - 10, y + 10);
	}

}