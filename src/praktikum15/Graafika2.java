package praktikum15;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Graafika2 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistamise nÃ¤ide");
		Group root = new Group();
		Canvas canvas = new Canvas(400, 400);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		
		
		
		Lumehelves2 l = new Lumehelves2(100, 100);
		l.joonistaMind(gc);
		
		Lumehelves2 l2 = new Lumehelves2(200, 200);
		l2.joonistaMind(gc);
		
		//joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {
		
		// MÃ¤Ã¤rame vÃ¤rvid
		gc.setFill(Color.GREEN);
		gc.setStroke(Color.BLUE);
		
		// Joone laius
		gc.setLineWidth(5);
		
		// Tegelase roheline pea
		gc.fillRoundRect(50, 50, 300, 300, 40, 40);
		
		// Suu
		gc.strokeLine(100, 300, 300, 300);
		
		// Silmad
		gc.strokeOval(100, 100, 50, 50);
		gc.strokeOval(250, 100, 50, 50);
		
		// VÃ¤rvivahetus
		gc.setFill(Color.RED);
		
		// Punane nina
		gc.fillRoundRect(175, 200, 50, 50, 10, 10);
		
		gc.fillText("Olen roheline mehike Marsilt", 100, 370);
	}
}