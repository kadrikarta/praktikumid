package praktikum15;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class JoonistuseMain extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistamise nÃ¤ide");
		Group root = new Group();
		Canvas canvas = new Canvas(400, 400);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		
		GraafilineLumesadu ls = new GraafilineLumesadu(50, gc);
		
//		Lumehelves l = new Lumehelves(100, 100);
//		l.joonistaMind(gc);
//		
//		Lumehelves l2 = new Lumehelves(200, 200);
//		l2.joonistaMind(gc);
		
		//joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}
	
}