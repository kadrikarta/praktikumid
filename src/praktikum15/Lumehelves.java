package praktikum15;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Lumehelves {

	int x, y;

	public Lumehelves(int x, int y) {

		this.x = x;
		this.y = y;
	}

	public void joonistaMind(GraphicsContext gc) {

		// Määrame värvid
		gc.setFill(Color.BLUE);
		gc.setStroke(Color.BLUE);

		// Joone laius
		gc.setLineWidth(1);

		// Suu
		gc.strokeLine(x + 10.0, y + 20.0, x + 30.0, y + 20.0); // horisontaalne joon
		gc.strokeLine(x + 15.0, y + 20.0, x + 12.5, y + 17.5);
		gc.strokeLine(x + 15.0, y + 20.0, x + 12.5, y + 22.5);
		gc.strokeLine(x + 25.0, y + 20.0, x + 27.5, y + 22.5);
		gc.strokeLine(x + 25.0, y + 20.0, x + 27.5, y + 17.5);
		gc.strokeLine(x + 12.5, y + 12.5, x + 27.5, y + 27.5);
		gc.strokeLine(x + 27.5, y + 12.5, x + 12.5, y + 27.5);
		gc.strokeLine(x + 20.0, y + 10.0, x + 20.0, y + 30.0); // vertikaalne joon
		gc.strokeLine(x + 20.0, y + 15.0, x + 17.5, y + 12.5);
		gc.strokeLine(x + 20.0, y + 15.0, x + 22.5, y + 12.5);
		gc.strokeLine(x + 20.0, y + 25.0, x + 17.5, y + 27.5);
		gc.strokeLine(x + 20.0, y + 25.0, x + 22.5, y + 27.5);
		// gc.fillText("Olen roheline mehike Marsilt", 100, 370);
	}
}
