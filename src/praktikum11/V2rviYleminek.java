package praktikum11;

//Kirjuta programm, mis täidab ekraani tausta sujuva üleminekuga valgest mustaks (üleval valge, all must).
//Taibudele: kirjuta universaalsem meetod, mis lubab ette anda mistahes kaks värvi: 

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class V2rviYleminek extends Applet {

	@Override
	public void paint(Graphics g) {
		// g.drawLine(100,100,200,200);

		int w = getWidth();
		int h = getHeight();
		int v2rviKood = 255;

		int y0 = 0;

		double v2rviMuutus = 255.0 / h;

		// Ta"idame tausta
		g.setColor(Color.white);
		g.fillRect(0, 0, w, h);

		for (int i = 0; i < h; i++) {
			v2rviKood = (int) (255 - v2rviMuutus * i); // v2rviKood - 255 / h;
			Color minuV2rv = new Color(v2rviKood, v2rviKood, v2rviKood);
			g.setColor(minuV2rv);
			g.drawLine(0, i, w, i);
		} // for

	}// public paint

}// klass
