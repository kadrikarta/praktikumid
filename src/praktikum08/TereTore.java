package praktikum08;

import lib.TextIO;

//Kirjutada programm, mis küsib kasutaja käest sõna ning trükib välja selle 
//sõna suurte tähtedega ning tähed sidekriipsudega eraldatud.
//Näiteks, kui kasutaja sisestab "Teretore", trükib programm "T-E-R-E-T-O-R-E".

public class TereTore {
	
	public static void main(String[] args) {
		
		System.out.println("Palun sisesta sõna: ");
		String s6na = TextIO.getln();
		String suurS6na = s6na.toUpperCase();
		
		for (int i = 0; i < suurS6na.length(); i++){
			if(i == 0){
				System.out.print(suurS6na.charAt(i));
			}else{
				System.out.print("-" + suurS6na.charAt(i));
			}
		}
		
	}

		
}
