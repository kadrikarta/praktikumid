package praktikum08;

import lib.TextIO;

//Kirjutada programm, mis küsib kasutaja käest kümme sõna 
//ja trükib välja sõna pikkuse ja sõna enda.

public class KymmeS6na {
	
	public static void main(String[] args){
	
		System.out.println("Palun sisesta sõna: ");
		String s6na = TextIO.getln();
		int pikkus = s6na.length();
	
	System.out.println("Sõna: " + s6na);
	System.out.println("Pikkus: " + pikkus);
	}
		
}
