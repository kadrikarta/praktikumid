package praktikum06;

import praktikum01.raamat.TextIO;
import praktikum05.Liisk;

//Kirjutada äraarvamismäng.
//Arvuti "mõtleb" ühe arvu ühest sajani.
//Küsib kasutajalt, mis number oli.
//Kui kasutaja vastab valesti, siis ütleb kas arvatud number oli suurem või väiksem.
//Programm küsib vastust senikaua kuni kasutaja numbri õigesti ära arvab.

public class ArvamisM2ng {

	public static void main(String[] args) {

		
		int kasutajaArv;
		int arvutiArv = Liisk.random(1, 100);

		do {
			System.out.print("Arva, mis number on.\nSisesta number ühest sajani: ");
			kasutajaArv = TextIO.getlnInt();
			//System.out.println(arvutiArv); //Selleks, et kontrollida arvuti genereeritud arvu.
			if (kasutajaArv < arvutiArv) {
				System.out.println("Otsitav arv on suurem. Paku uus arv: ");
			} else if (kasutajaArv > arvutiArv) {
				System.out.println("Otsitav arv on väiksem. Paku uus arv: ");

			} else if (kasutajaArv == arvutiArv){
				System.out.println("Arvasid õigesti! Otsitud arv on " + arvutiArv);
			}
		} while (kasutajaArv != arvutiArv);
	}
}
