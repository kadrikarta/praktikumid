package praktikum06;

import praktikum01.raamat.TextIO;
import praktikum05.KullKiri;

//Kirjutada panustega kulli ja kirja mäng.
//Kasutajale antakse 100 raha.
//Küsitakse panuse suurust, maksimaalne panus on 25 raha.
//Visatakse münt.
//Kui tuli kiri, saab kasutaja panuse topelt tagasi.
//Kui tuli kull, ei saa ta midagi.
//Mäng kestab seni, kuni kasutajal raha otsa saab.
//NB! Mõistlik on kasutajale raha jääki vahel näidata ka.

public class KullKiriPanused {

	public static void main(String[] args) {

		System.out.println("Mängime kulli ja kirja!\n Sul on 100 raha, maksimaalne panus on 25");
		System.out.println("Kui võidad siis saad panuse kahekordselt tagasi.\n Mäng kestab, kuni su raha otsa saab.");

		int panus;
		int raha = 100;

		while (raha > 0) {
			do {
				System.out.println("Pane  panus (max 25)");
				panus = TextIO.getlnInt();
			} while (panus > 25 || panus < 1);

			int tulemus = kullKiri();
			// System.out.println(tulemus);
			// 1 on võit ja 2 on kaotus

			if (tulemus == 1) {
				raha = raha + panus;
			}
			if (tulemus == 2) {
				raha = raha - panus;
			}
			System.out.print("Sul on nüüd " + raha + " raha.");
			if (raha == 0 || raha < 0) {
				System.out.print("Mäng läbi!");
			}
		}

	}// main


	public static int kullKiri() {

		int kasutajaArv = KullKiri.kasutajaSisestus("Kas kull(0) või kiri(1)?", 0, 1);
		if (kasutajaArv == 0) {
			System.out.println("Sina valisid kulli.");
		}
		if (kasutajaArv == 1) {
			System.out.println("Sina valisid kirja.");
		}
		// Ütlen, mille valis arvuti
		int tulemus = KullKiri.juhuArv(0, 1);
		if (tulemus == 0) {
			System.out.println("Tuli kull.");
		}
		if (tulemus == 1) {
			System.out.println("Tuli kiri.");
		}
		// Ütlen tulemuse
		if (kasutajaArv == tulemus) {
			System.out.println("Sina võitsi!");
			return 1;
		} else {
			System.out.println("Kahjuks kaotasid.");
			return 2;
		}
	}

} // klass
