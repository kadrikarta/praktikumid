package praktikum14;

//Luua klass Joon, mis koosneks kahest punktist. 
//Joon klassil kirjuta samuti üle toString meetod ning ühtlasi lisa temale ka meetod, 
//mis tagastab joone pikkuse, näiteks: public double pikkus()

public class Joon {

	Punkt punktYks;
	Punkt punktKaks;

	public Joon(Punkt p1, Punkt p2) {

		punktYks = p1;
		punktKaks = p2;

	}

	public String toString() {

		return "Joon(" + punktYks + ", " + punktKaks + ")";
	}

	public double pikkus(Punkt algus, Punkt l6pp) {

		double a = l6pp.y - algus.y;
		double b = l6pp.x - algus.x;
		double c = Math.sqrt(a * a + b * b);

		return c;
	}
}
