package praktikum14;

//Luua klass Ring, mis koosneks keskpunktist ja raadiusest. 
//Ring klassil olgu meetodid, mis arvutavad ümbermõõdu ning pindala.
//Ümbermõõt = 2*pii*r
//Pindala = pii*r^2

public class Ring {

	Punkt keskPunkt;
	double r;

	public Ring(Punkt kese, double raadius){
		
		keskPunkt = kese;
		r = raadius;
	}
	
	public double ymberm66t() {

		return Math.PI * r * 2;
	}

	public double pindala() {

		return Math.pow(r, 2) * Math.PI;
	}
	
	@Override
	public String toString(){
		return "Ring, ümbermõõt on: " + ymberm66t()
		+ ", pindala on: " + pindala();
	}
}
