package praktikum14;

//Luua klass Silinder, mis laiendaks (extend) Ring klassi. 
//Silindril olgu meetod, mis arvutab pindala ja ruumala.

public class Silinder extends Ring{
	
	double h;
		
	public Silinder(Ring r, double k6rgus) {
		super(r.keskPunkt, r.r);
		h = k6rgus;
	}

	@Override
	public String toString() {
		
		return "Tegelt olen silinder " + super.toString();
	}
	
	
	public double pindala(){
		
		return super.pindala() *2 + super.ymberm66t() * h;
	}
	
	public double ruumala(){
	
		return super.pindala() * h;
	}

}
