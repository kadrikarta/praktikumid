package praktikum14;

//Luua klass Punkt, mille objektidel oleks kaks omadust: x-koordinaat ja y-koordinaat. 
//Kirjutada üle ka Punkt klassi toString() meetod. "main"-meetodit ei ole sellele klassile 
//mõistlik teha, kuid tema testimise ja katsetamiste jaoks loo üks eraldiseisev klass 
//(näiteks Katsetused-nimeline).

public class Punkt {

	int x, y; // kaks int'i

	public Punkt() {

	}

	public Punkt(int x, int y) {

		this.x = x;
		this.y = y;

	}

	// toString prindib välja asju.
	@Override
	public String toString() {
		return "Punkt(" + x + ", " + y + ")";
	}

}
