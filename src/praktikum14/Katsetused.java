package praktikum14;

public class Katsetused {
	
	public static void main (String[] args){
		
		Punkt minuPunkt = new Punkt();
		minuPunkt.x = 100;
		minuPunkt.y = 100;
		
		Punkt veelYksPunkt = new Punkt(200, 200);
		
		System.out.println(minuPunkt);
		System.out.println(veelYksPunkt);
		
		Ring ringYks = new Ring(minuPunkt, 50.);
		System.out.println(ringYks);
		
		Silinder minuSilinder = new Silinder(ringYks, 34.);
		System.out.println(minuSilinder);
		
		
		
	}

}
