package praktikum09;

import lib.TextIO;

public class Inimene { //extends Object alati vaikimisi ka seal

	//Kuuluvad klassi juurde. Main meetodit ei ole ju :)
	String nimi; 
	int vanus; //objekti muutujad


	//konsruktor on alati sama nimega mis klass ja public! lihtsalt public...või private....
	public Inimene(String nimi, int vanus) { //Suure algustähega, sest see on konstruktor-meetod
		this.nimi = nimi;					 //Kutsutakse välja kui programm käivitatakse, antakse ette kaks	
		this.vanus = vanus;					 //argumenti: nimi ja vanus
	} //this: viide objektile (2 väärtust: nimi ja vanus). Viitab objekti "nimele".
	//this viitab objektile mis just loodi.		

	public boolean equals(Inimene teine) {
		return teine.vanus == this.vanus && this.nimi.equals(teine.nimi); //kontrollib, kas on sama inimene
	}

	public void tervita() { //meetod!!! Javas ongi ainult meetodid :D
		TextIO.putln("Tere, minu nimi on " + nimi + ", olen " + vanus + "-aastane.");
	}

	// @ koodis on annotatsioon. 
	// Otseselt ei muuda programmi tööd. Annab editorile teada, et kirjutab midagi üle.
	// kirjuab üle toString meetodi mingist teisest klassist.
	// Annotatsiooni ei pea sinna panema. Aga oleks mõistlik.
	@Override
	public String toString() {
		return nimi + " " + vanus;
	}
}
