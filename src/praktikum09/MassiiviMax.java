package praktikum09;

//Leida programselt ühemõõtmelise massiivi suurim element.

public class MassiiviMax {

	public static void main(String[] args) {

		int[] massiiv = { 1, -3, 6, 7, 8, 3, 5, 7, -21, 3 };
		System.out.println("Suurim element on: " + suurim(massiiv));
	}

	public static int suurim(int[] sisend) {

		// int max = Integer.MIN_VALUE; //kõige väiksem int tüüpi arv, selleks et töötaks kas negatiivsete arvudega.
		int max = sisend[0]; //mulle meeldib see rohkem :) õppejõud ütles et teine on parem :P
		for (int i : sisend) {
			if (i > max) {
				max = i;
			}

		}

		return max;
	}

}
