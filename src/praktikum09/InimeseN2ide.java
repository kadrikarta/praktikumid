package praktikum09;

import java.util.ArrayList;

import praktikum01.raamat.TextIO;

public class InimeseN2ide {
	
	public static void main(String[] args){
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		
		System.out.println("Palun sisesta nimi ja vanus: ");
		String nimi = TextIO.getlnString();
		int vanus = TextIO.getlnInt();
		Inimene keegi = new Inimene(nimi, vanus);
		inimesed.add(keegi);
		
		
		inimesed.add(new Inimene("Mati", 34));
		

		for (Inimene inimene : inimesed) { //lühike for tsükkel: for each. Käib läbi kõik arraylisti elemendid.
										   //võtab ühekaupa inimesi. Inedeks pole oluline. me ei tea kus konkreetne
										   //inimene massiivis asub. Võib kasutada ka pikka, aga lühike on loetavam
		    // Java kutsub välja Inimene klassi toString() meetodi
		    System.out.println(inimene);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//	String nimi = new String("Mati");
//	
//	Inimene keegi = new Inimene("Kati", 34);
//	
//	System.out.println(keegi.vanus);
//	keegi.tervita();
//	
//	Inimene keegiVeel = new Inimene("Mati", 18);
//	keegiVeel.tervita();
//	
	}

}
