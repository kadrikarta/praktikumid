package praktikum12;

//Võttes sisendandmeiks failis Matrix.java leiduva 
//ruutmaatriksi neo, lahendada järgmised ülesanded.

public class Massiivid {

	public static void main(String[] args) {

		int[] minuMassiiv = { 2, 3, 4, 5, 6 };
		
		System.out.println("Ühemõõtmeline massiiv: ");
		tryki(minuMassiiv);

		int[][] neo = { 
						{ 1, 1, 1, 1, 1 }, 
						{ 2, 3, 4, 5, 6 }, 
						{ 3, 4, 5, 6, 7 }, 
						{ 4, 5, 6, 7, 8 },
						{ 5, 6, 7, 8, 9 }, 
		};
		System.out.println();
		System.out.print("Kahemõõtmeline massiiv: ");
		tryki(neo);
		ridadeSummad(neo);
		korvalDiagonaaliSumma(neo);
		ridadeMaksimumid(neo);
		miinimum(neo);
		
		}


	// 1.Kirjutada meetod, mis trükib ekraanile ühel real parameetrina etteantud
	// ühemõõtmelise täisarvumassiivi elemendid
	public static void tryki(int[] massiiv) {

		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}

	}

	// 2.Kirjutada meetod, mis trükib ekraanile parameetrina etteantud
	// kahemõõtmelise massiivi (maatriksi)
	// Kasuta maatriksi rea trükkimiseks kindlasti eelnevalt kirjutatud
	// meetodit!
	public static void tryki(int[][] maatriks) {

		// Trykime maatriksi va"lja
		for (int i = 0; i < maatriks.length; i++) {
			System.out.println();
			tryki(maatriks[i]);
					}
		System.out.println();

	}

	// Arvutada maatriksi iga rea elementide summa
	public static int[] ridadeSummad(int[][] maatriks) {
		
		//parameetrina antud maatriksi ridade arv
		int[] reaSummad = new int[maatriks.length]; 

		for (int i = 0; i < maatriks.length; i++) {
			reaSummad[i] = reaSumma(maatriks[i]);			
		}
		System.out.print("Rea summad on: ");
		tryki(reaSummad);
		System.out.println();
		return reaSummad;
	}

	public static int reaSumma(int[] rida) {
		int summa = 0;

		for (int i : rida) {
			summa += i;			
		}
		//Kontrollimiseks:
		//System.out.println("Rea summa: " + summa);
		return summa;
	}
	
	//4.Arvutada kõrvaldiagonaali elementide summa 
	//(kõrvaldiagonaal on see, mis jookseb ülevalt paremast nurgast alla vasakusse nurka). 
	
	public static int korvalDiagonaaliSumma(int[][] maatriks){
		System.out.println("------Kõrvaldiagonaalisumma------");
		//Milline maatriks on?
		//tryki(maatriks);
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++){
			for(int j = 0; j < maatriks[i].length; j++){
				if (i + j == maatriks.length -1)
				summa += maatriks[i][j];
				//Kontrolli, millega i ja j võrduvad:
				//System.out.print("(i=" + i + "j=" + j + ")");
			}
		}
		System.out.println(summa);
	return summa;
	}
	
	//5.Leida iga rea suurim element. 
	public static int[] ridadeMaksimumid(int[][] maatriks){
		System.out.println("------Ridade maksimumid------");
		
		int[] reaMaxid = new int[maatriks.length]; 
		
		for (int i = 0; i < maatriks.length; i++){
			reaMaxid[i] = reaMaksimum(maatriks[i]);	
		}
		
		tryki(reaMaxid);
		return reaMaxid;
	}
	
	public static int reaMaksimum(int[] rida) {
		
		int max = Integer.MIN_VALUE;

		for (int i : rida) {
			if (i > max) {
				max = i;
			}
		}
		//Kontrollimiseks:
		System.out.println("Rea maksimum: " + max);
		return max;
	}
	
	
	//6.Leida kogu maatriksi väikseim element. 
	public static int miinimum(int[][] maatriks){
		System.out.println();
		System.out.println("------Maatriksi miinimum------");
	  int[] miinimumid = new int[maatriks.length];
	  
	  	for ( int i = 0; i < maatriks.length; i++){
	  		miinimumid[i] = reaMiinimum(maatriks[i]);
	  	}
	  	
	  	System.out.println(reaMiinimum(miinimumid));
		
	return reaMiinimum(miinimumid);
	}
	
	public static int reaMiinimum(int[] rida){
		
		int min = Integer.MAX_VALUE;
		for (int i : rida) {
			if (i < min) {
				min = i;
			}
		}
		
		return min;
	}
	
	
}// klass
