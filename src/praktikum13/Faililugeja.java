package praktikum13;

//Kirjutada programm, mis loeb tekstifailist read, 
//sorteerib need tähestikulisse järjekorda ja trükib välja. 

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class Faililugeja {
	
	public static void main(String[] args) {

		// punkt tähistab jooksvat kataloogi
		String kataloogitee = Faililugeja.class.getResource(".").getPath();

		ArrayList<String> failiSisu = loeFail(kataloogitee + "Kala.txt");
		System.out.println(failiSisu);
		Collections.sort(failiSisu);
		System.out.println(failiSisu);
	}

	public static ArrayList<String> loeFail(String failinimi) {

		// otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(failinimi);

		ArrayList<String> failiRead = new ArrayList<String>();

		try {
			// avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			// buffered readerile antakse ette file reader objekt, millele
			// antakse ette fail.
			String rida;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				// System.out.println(rida);
				failiRead.add(rida);

			}
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		return failiRead;
	}

}// klass
