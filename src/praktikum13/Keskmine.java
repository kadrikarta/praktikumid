package praktikum13;

//Kirjutada programm, mis loeb tekstifailist hulga numbreid (iga number on eraldi real), 
//arvutab nende aritmeetilise keskmise ja trükib selle ekraanile.
//Vihje: Stringist numbri saab nii:
//double nr = Double.parseDouble(line);

import java.util.ArrayList;

public class Keskmine {
	
	public static void main(String[] args){
		
		// punkt tähistab jooksvat kataloogi
		String kataloogitee = Faililugeja.class.getResource(".").getPath();
		ArrayList<String> failiSisu = Faililugeja.loeFail(kataloogitee + "Numbrid.txt");
		System.out.println(failiSisu);
		
		System.out.println(leiaKeskmine(failiSisu));
	}
	
	public static double leiaKeskmine(ArrayList<String> algAndmed){
		
		double elemente = algAndmed.size();
		double summa = 0;
		double vigasedRead = 0;
		
		for (String i : algAndmed){
			try{
			double nr = Double.parseDouble(i);
			summa += nr;
			}catch (NumberFormatException e){
				System.out.println("Vigane rida: " + i);
				vigasedRead++;
			}
		}
		elemente = elemente - vigasedRead;
		
		double keskmine = summa / elemente; 
		
		return keskmine;
	}

}
