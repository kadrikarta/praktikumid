package praktikum02;

import lib.TextIO;

public class Korrutamine {
	public static void main(String[] args) {

		int esimeneNumber; // kasutaja sisestatud number
		int teineNumber; // teine input
		int korrutis; // sisestatud arvude korrutis

		System.out.print("Sisesta esimene number, mida soovid korrutada");
		esimeneNumber = TextIO.getlnInt();
		System.out.print("Sisesta teine number, mida soovid korrutada");
		teineNumber = TextIO.getlnInt();

		korrutis = esimeneNumber * teineNumber;

		System.out.println();
		System.out.println("Sa sisestasid " + esimeneNumber + " ja " + teineNumber);
		System.out.println("Nende arvude korrutis on " + korrutis);
		System.out.println();

	} // maini lõpp

} // klassi lõpp
