package praktikum02;

import lib.TextIO;

public class J22gigaJagamine {
	public static void main(String[] args) {

		int inimesed;
		int grupp;
		int gruppeKokku;

		System.out.print("Sisesta inimeste arv ");
		inimesed = TextIO.getlnInt(); // millega jagatakse
		System.out.print("Sisesta grupi suurus ");
		grupp = TextIO.getlnInt(); // millega jagad

		gruppeKokku = inimesed / grupp;

		int grupita; //ütlen et on selline muutuja olemas! Väga oluline :P
		grupita = inimesed % grupp;  // Väljastab ainult jagamise jäägi
									
		System.out.println();
		System.out.println("Seega inimesi on " + inimesed + " ja gruppi mahub " + grupp);
		System.out.println("Nii tekib " + gruppeKokku + " gruppi");
		System.out.println(grupita + " inimest ei mahu gruppi");
		System.out.println();

	}
}