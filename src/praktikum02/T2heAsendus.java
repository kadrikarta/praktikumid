package praktikum02;

import lib.TextIO;

public class T2heAsendus {
	public static void main(String[] args) {
		
		System.out.print("Sisesta tekst, mis sisaldab häälikut 'a': ");
		String tekst = TextIO.getln();
		
		String uusTekst;
		uusTekst = tekst.replace('a', '_');
		
		System.out.print(uusTekst);
		
		
	}

}
