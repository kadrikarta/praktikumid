package praktikum05;

import praktikum01.raamat.TextIO;

public class SisestusMeetod {

	public static void main(String[] args) {
		System.out.print(kasutajaSisestus(1, 10));

	}// main

	public static int kasutajaSisestus(int min, int max) {

		int arv;

		do {
			System.out.print("Sisesta arv vahemikus " + min + " kuni " + max + ": ");
			arv = TextIO.getlnInt();
		} while (arv < min || arv > max);// sest ma tahan tsüklisse minna ju kui
											// pakutakse vale arv!!!!

		return arv;

	}

	public static void kasutajaSisestus(String string, int i, int j) {
		
	
		
	}
}
