package praktikum05;

//Kirjutada liisu tõmbamise programm.
//Küsida kasutajalt inimeste arv. 
//Väljastada random number vahemikus 1 kuni arv (kaasaarvatud)
//NB! Kontrollida, et töötab õigesti: st. öeldes mitu korda järjest arvuks 3, 
//peab võimalike vastuste hulgas olema nii ühtesid, kahtesid kui kolmi.

import java.util.Random;

import praktikum01.raamat.TextIO;

public class Liisk {

	public static void main(String[] args) {

		int osalejad;
		do {
			System.out.print("Palun sisesta osalejate arv: ");
			osalejad = TextIO.getInt();
		} while (osalejad <= 0);

		System.out.print("Liisk langes osaleja number " + random(1, osalejad) + ".");

	}

	public static int random(int min, int max) {

		Random foo = new Random();
		int randomNumber = foo.nextInt((max + 1) - min) + min;

		return randomNumber;
	}

}
