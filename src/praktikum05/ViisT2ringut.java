package praktikum05;

//Kirjutada programm, mis viskab 5 tהringut ning arvutab silmade summa
//NB! Lahendus peab olema tehtud tskliga.
//Vihje: tsklis summa arvutamiseks tuleb summat sisaldav muutuja enne tskli
//kהivitamist vההrtustada nulliga ja tskli kehas liita sellele ks haaval vההrtusi otsa (sum = sum + ...)

public class ViisT2ringut {

	public static void main(String[] args) {

		int summa = 0;

		for (int i = 0; i < 5; i++)
					{
			summa = summa + KaheT2ringuM2ng.t2ring();

		}
		System.out.println("Viskasid kokku: " + summa);

	}

}
