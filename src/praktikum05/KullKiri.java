package praktikum05;

import praktikum01.raamat.TextIO;

public class KullKiri {

	public static void main(String[] args) {
		// Ütlen, mille valis kasutaja
		int kasutajaArv = kasutajaSisestus("Kas kull(0) või kiri(1)?", 0, 1);
		if (kasutajaArv == 0) {
			System.out.println("Sina valisid kulli.");
		}
		if (kasutajaArv == 1) {
			System.out.println("Sina valisid kirja.");
		}
		// Ütlen, mille valis arvuti
		int tulemus = juhuArv(0, 1);
		if (tulemus == 0) {
			System.out.println("Tuli kull.");
		}
		if (tulemus == 1) {
			System.out.println("Tuli kiri.");
		}
		// Ütlen tulemuse
		if (kasutajaArv == tulemus) {
			System.out.println("Sina võitsi!");
		} else {
			System.out.println("Kahjuks kaotasid.");
		}
	} // main

	public static int kasutajaSisestus(String kysimus, int min, int max) {
		int sisestus;
		do {
			System.out.print("Palun vali, kas kull(0) või kiri(1).\n Sisesta vastav number: ");
			sisestus = TextIO.getlnInt();
		} while (sisestus < min || sisestus > max);
		return sisestus;

	}// kasutajaSisestus

	// See meetod leiab juhusliku arvu 0 ja 1 vahel
	public static int juhuArv(int min, int max) {
		double juhuslik = Math.random() * 100;
		int k = (int) juhuslik % 2;
		if (k == 0) {
			int arvutiArv = 0;
			return arvutiArv;
		} else {
			int arvutiArv = 1;
			return arvutiArv;
		}
	}

}// klass
