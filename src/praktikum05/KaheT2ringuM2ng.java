package praktikum05;

//Kirjutada täringumäng:
//Programm viskab kaks täringut mängijale ja kaks täringut endale (arvutile), 
//arvutab mõlema mängija silmade summad ja teatab, kellel oli rohkem.

import java.util.Random;

public class KaheT2ringuM2ng {

	public static void main(String[] args) {
		// kaks korda tuleb täringu meetod välja kutsuda ja tulemusi võrrelda

		int m2ngija = t2ring() + t2ring();
		System.out.println("Viskasid kokku: " + m2ngija);
		int arvuti = t2ring() + t2ring();
		System.out.println("Arvuti viskas: " + arvuti);

		if (m2ngija > arvuti) {
			System.out.println("Sina võitsid!");
		} else if (arvuti > m2ngija) {
			System.out.println("Arvuti võitis!");
		} else {
			System.out.println("Mäng jäi viiki!");
		}

	}// main

	public static int t2ring() {

		int min = 1;
		int max = 6;

		Random foo = new Random();
		int randomNumber = foo.nextInt((max + 1) - min) + min;

		return randomNumber;
	}

}// klass
