package praktikum10;

import lib.TextIO;

//Kirjuta meetod, mis leiab rekursiivselt etteantud järjekorranumbriga n Fibonacci arvu.
//Täiendus taibudele: kirjuta efektiivsem fibonacci meetod. Rekursiooni kasutamine ei ole vajalik.

public class Fibonacci {

	public static void main(String[] args) {
		
		System.out.println("Mitmendat fibonacci arvu soovid leida?");
		System.out.println("Sisesta arv: ");
		int koht = TextIO.getlnInt();
				
		System.out.println("Fibbonacci arv kohal " + koht + " on " + fibonacci(koht));
	}

	public static long fibonacci(int n) {
		
		if (n == 0)
			return 0;
		else if (n == 1 || n == 2) {
			return 1;
		} else
			return fibonacci(n - 1) + fibonacci(n - 2);
	}

}
