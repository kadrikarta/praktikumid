package praktikum10;

public class Rekursioon {

	public static void main(String[] args) {

		System.out.println(astenda(2, 3));
		// astenda (2,3) => 2 * astenda(2,2)
		// astenda (2,2) => 2 * astenda(2,1)
		// astenda (2,1) => 2
	}

	public static int astenda(int arv, int aste) {
		if (aste == 1) {
			return arv;
		} else
			return arv * astenda(arv, aste - 1);

	}// astenda

}
