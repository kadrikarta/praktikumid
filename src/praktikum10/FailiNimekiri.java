package praktikum10;

//Täiendada FailiNimekiri.trykiFailid meetodit niimoodi, 

//et trükitaks välja kataloogipuu rekursiivselt 
//(ka failid, mis asuvad alamkataloogides). 
//Trüki välja kõikide Eclipse Praktikumid-projekti koodifailide asukohad.

import java.io.File;
import java.util.Arrays;

public class FailiNimekiri {

	public static void trykiFailid(String kataloogiTee) {

		File kataloog = new File(kataloogiTee);
		File[] failid = kataloog.listFiles();
		Arrays.sort(failid); // tähestiku järjekorda

		for (File file : failid) { // käib kõik faili ükshaaval läbi, massiivist
									// failid
			if (file.isDirectory()) {
				System.out.println("Kataloog: " + file.getName());//või getAbsolutePath
				//System.out.println();
				trykiFailid(file.getAbsolutePath());
			} else {
				System.out.print("Fail:     ");
				System.out.println(file.getName());
			}
		}
	}

	public static void main(String[] args) {
		trykiFailid("/home/kkartase/Documents/git/praktikumid");
	}

}
