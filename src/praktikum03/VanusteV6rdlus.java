package praktikum03;

import lib.TextIO;

public class VanusteV6rdlus {

	public static void main(String[] args) {

		System.out.print("Sisesta esimese kasutaja vanus: ");
		Double a = TextIO.getlnDouble(); // a - esimene vanus

		System.out.print("Sisesta teise kasutaja vanus: ");
		Double b = TextIO.getlnDouble(); // a - teine vanus

		double vanuseVahe = Math.abs(a - b);

		if (vanuseVahe > 10) {
			System.out.print("Ei ole üldse sobilik!");
		} else if (vanuseVahe > 5) {
			System.out.print("Ei ole sobilik!");
		} else if (vanuseVahe < 5) {  // siia võib panna ka lihtsalt else.
			System.out.print("Sobib!");
		}

	}

}
