package praktikum03;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		System.out.print("Sisesta keskmine hinne: ");
		double hinne = TextIO.getlnDouble();
		
		System.out.print("Sisesta lõputöö hinne: ");
		double thinne = TextIO.getlnDouble();

		if (hinne < 4.5) {
			System.out.print("Ei saa!");
		} else if (thinne == 5.0) {
			System.out.print("Saad Cum Laude diplomi!");
		} else {
			System.out.print("Ei saa!");
		}

	}
}