package praktikum03;

import lib.TextIO;

public class CumLaude1 {

	public static void main(String[] args) {

		System.out.print("Sisesta keskmine hinne: ");
		double hinne = TextIO.getlnDouble();

		if (hinne < 0 || hinne > 5) {
			System.out.print("Vigane hinne!");
			return;
		}
		System.out.print("Sisesta lõputöö hinne: ");
		double thinne = TextIO.getlnDouble();

		if (thinne < 0 || thinne > 5) {
			System.out.print("Vigane hinne!");
			return;
		}

		if (hinne > 4.5 && thinne == 5) {			 // && loogiline JA
			System.out.print("Saad Cum Laude1");
		} else {
			System.out.print("Ei saa!");
		}
	}

}
