package praktikum03;

import lib.TextIO;

public class Parool {

	public static void main(String[] args) {

		String oigeParool;
		oigeParool = "par00lih2rjutys";

		System.out.print("Sisesta parool: ");
		String parool = TextIO.getlnString();

		if (oigeParool.equals(parool)) {  //ei saa panna == !!!!!
			System.out.print("Õige parool!");
		} else {
			System.out.print("Vale parool!");
			return;
		}

	}

}
