package praktikum03;

import lib.TextIO;

public class ParoolWhile {

	public static void main(String[] args) {

		String oigeParool;
		oigeParool = "par00lih2rjutys";

		int sisestusi = 0;

		while (true) {
			System.out.print("\nSisesta parool: ");
			String parool = TextIO.getlnString();

			if (oigeParool.equals(parool)) { // ei saa panna == !!!!!
				System.out.print("Õige parool!");
				break;
			} else {
				System.out.print("Vale parool!");
				sisestusi = sisestusi + 1;
			}

			if (sisestusi >= 3) {
				System.out.print("\nKolm vale katset!");
				return; // lõpetab programmi töö
			}

		}
	}

}
