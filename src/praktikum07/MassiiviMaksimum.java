package praktikum07;

//Kirjutada meetod, mis leiab ja tagastab ettantud kahemõõtmelise massiivi maksimaalse elemendi. 
//Kirjuta kõigepealt meetod, mis leiab ühemõõtmelise massiivi maksimaalse elemendi ning 
//kasuta siis seda meetodit teises: 

public class MassiiviMaksimum {

	public static void main(String[] args) {
		System.out.println("Ühemõõtmelise masiivi maksimum: " + maksimum(new int[] { 0, 5, 6, 9, 2, 4, 5, 1, 9, 2 }));
		System.out.println(
				"Kahemõõtmelise massiivi maksimum: " + maksimum(new int[][] { { 2, 8, 0 }, { 5, 3, 1 }, { 7, 4, 6 } }));
	}// main

	// meetod, mis leiab ühemõõtmelise massiivi maksimumi
	public static int maksimum(int[] massiiv) {

		int max = 0;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > max) {
				max = massiiv[i];
			}
		}

		return max;
	}

	// meetod, mis leiab maatriksi maksimumi
	public static int maksimum(int[][] maatriks) {
		// maatriksi rea maksimumi leidmiseks saad siin edukalt kasutada eelmist
		// meetodit

		int matMax = 0;
		for (int i = 0; i < maatriks.length; i++) {
			int reaMax = maksimum(maatriks[i]);
			System.out.println("Rea maksimum on: " + reaMax);
			if (reaMax > matMax) {
				matMax = reaMax;
			}
		}

		return matMax;
	}

}// klass
