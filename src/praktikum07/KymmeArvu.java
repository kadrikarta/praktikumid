package praktikum07;

import praktikum01.raamat.TextIO;

//Kirjutada programm, mis küsib kasutajalt 10 arvu 
//ning trükib nad seejärel vastupidises järjekorras ekraanile.

public class KymmeArvu {

	public static void main(String[] args) {

		int[] arvud = new int[10];
		// Näide massiivi väärtustamisest.
		// int[] arvud = {3, 5, 7, 9}
		// arvud[0] = 3;
		// System.out.println(arvud[1]
		// for (int i = 0; i < arvud.length; i++) {
		// System.out.println(arvud[i]);
		// }

		System.out.println("Küsin sult 10 arvu.");
		for (int i = 0; i < 10; i++) {
			System.out.print("Sisesta arv: ");
			arvud[i] = TextIO.getlnInt();
		}
		System.out.print("Sisestasid: ");
		for (int i = 0; i < arvud.length; i++) {
			System.out.print(arvud[i] + " ");
		}
		System.out.println();

		int[] teistPidi = reverse(arvud);

		System.out.print("Vastupidises järjekorras on see: ");
		for (int j = 0; j < teistPidi.length; j++) {
			System.out.print(teistPidi[j] + " ");

		}

	}// main

	public static int[] reverse(int[] m) {
		int[] res = new int[m.length];
		for (int i = 0; i < m.length; i++) {
			res[res.length - 1 - i] = m[i];
		}
		return res;
	} // reverse

}// klass
