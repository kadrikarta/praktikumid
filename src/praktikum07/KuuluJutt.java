package praktikum07;

//Kirjutada kuulujutugeneraator.
//Kolm ühepikkust massiivi: Esimeses naisenimed, teises mehenimed, kolmandas tegusõnad.
//Programm võtab igast massiivist ÜHE suvalise elemendi ja kombineerib nendest lause.
//Vihje: Massivist suvalise elemendi valimiseks arvuta random number 
//vahemikus 0 kuni massiivi pikkus (random * length).

// Näide massiivi väärtustamisest.
// int[] arvud = new int[10];
// int[] arvud = {3, 5, 7, 9}
// arvud[0] = 3
// System.out.println(arvud[1]
// for (int i = 0; i < arvud.length; i++) {
// System.out.println(arvud[i]);
// }

public class KuuluJutt {

	public static void main(String[] args) {

		String[] naised = { "Kati ", "Mari ", "Maali ", "Juta " };
		String[] mehed = { "Toomas", "Peeter", "Marko", "Hardi" };
		String[] verbid = { "ootab ", "kardab ", "vaatab ", "usaldab " };

		System.out.print(juhuLiige(naised));
		System.out.print(juhuLiige(verbid));
		System.out.print(juhuLiige(mehed) + "t.");

	}// main

	public static String juhuLiige(String[] m) {
		return m[(int) (Math.random() * m.length)];
	}

}// klass
