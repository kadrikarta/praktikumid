package praktikum04;

//Tabel, mille moodustab:
// rida  nullidest ja ühest ühest, kus 1 on rea järjekorranumbriga liige + reavahetus

public class Tabel1 {

	public static void main(String[] args) {

		int tabeliSuurus;
		tabeliSuurus = 7;

		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if (j == i){
					System.out.print("1 ");
				}else{
					System.out.print("0 ");
				}
			}
			System.out.println();
		}
	}
}
